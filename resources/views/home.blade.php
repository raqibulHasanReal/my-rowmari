@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">       
        <div class="col-md-4">
            <div class="animatebox animate rotateInDownLeft">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Intro of Rowmari</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="#">More Details</a></footer>
                    </article>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animatebox animate fadeInDownBig">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Blogs</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="/posts">More Details</a></footer>
                    </article>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animatebox animate rotateInDownRight">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Intro of Rowmari</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="#">More Details</a></footer>
                    </article>
                    
                </div>
            </div>            
        </div>
    </div>
    <div class="row justify-content-center ">
        <div class="col-md-4">
            <div class="animatebox animate rotateInUpLeft">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Intro of Rowmari</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="#">More Details</a></footer>
                    </article>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animatebox animate fadeInUpBig">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Intro of Rowmari</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="#">More Details</a></footer>
                    </article>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="animatebox animate rotateInUpRight">
                <div id="introblocks">
                    
                    <article>
                        <div>
                            <h6 class="heading">Intro of Rowmari</h6>
                            <p>Ex mauris faucibus libero  &hellip;</p>
                        </div>
                        
                        <footer style="color:aquamarine;background-color:bisque"><a href="#">More Details</a></footer>
                    </article>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
