<nav class="navbar navbar-expand-md navbar-light navbar-laravel navbar_posission_fixed nav-color">
	<div class="container">
		<a class="navbar-brand" href="{{ url('/') }}">
			{{ config('app.name', 'Laravel') }}
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Left Side Of Navbar -->
			<div id="hornav" class="">
				<ul class="navbar-nav mr-auto visible-lg">
						<li><a href="/home">Home</a></li>
					<li><span>Blog</span>
						<ul>
							<li><a href="/posts">Blog</a></li>
							<li><a href="/posts/create">Write your Blog</a></li>
						</ul>
					</li>
					
					<li><span>Features</span>
						<ul>
							<li class="parent"><span>Typography</span>
								<ul>
									<li><a href="#">Basic Typography</a></li>
									<li><a href="#">Blockquotes</a></li>
								</ul>
							</li>
							<li class="parent"><span>Components</span>
								<ul>
									<li><a href="#">Labels</a></li>
									<li><a href="#">Progress Bars</a></li>
									<li><a href="#">Panels</a></li>
									<li><a href="#">Pagination</a></li>
								</ul>
							</li>
							<li class="parent"><span>Icons</span>
								<ul>
									<li><a href="#">Icons General</a></li>
									<li><a href="#">Social Icons</a></li>
									<li><a href="#">Font Awesome</a></li>
									<li><a href="#">Glyphicons</a></li>
								</ul>
							</li>
							
							<li><a href="#">Testimonials</a></li>
							<li><a href="#">Accordions & Tabs</a></li>
							<li><a href="#">Buttons</a></li>
							<li><a href="#">Carousels</a></li>
							<li><a href="#">Grid System</a></li>
							<li><a href="#">Animate On Scroll</a></li>
						</ul>
					</li>

				    <li><span>pages</span>
						<ul>
							<li><a href="/about">about</a></li>
							<li><a href="/services">services</a></li>
						</ul>
					</li>
					
				    <li><a href="#">Profile</a></li>
				</ul>
			</div>

			{{--  <ul class="navbar-nav ml-auto">
					<!-- Authentication Links -->
					@guest
						<li class="nav-item">
							<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@else
						<li class="nav-item dropdown">
							<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('logout') }}"
									onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</li>
					@endguest
				</ul>  --}}
			
				<div id="hornav" class="">
				<!-- Right Side Of Navbar -->
				<ul class="navbar-nav ml-auto">
					<!-- Authentication Links -->
					@guest
						<li class="nav-item">
							<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@else
						<li class="nav-item dropdown">
							<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('logout') }}"
									onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</li>
					@endguest
				</ul>
			</div>
		</div>
	</div>
</nav>