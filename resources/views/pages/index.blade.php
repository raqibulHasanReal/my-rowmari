@extends('layouts.app')

@section('content')
<div class="jumbotron text-center">
    
    <h1>{{ $title }}</h1>
    <p>This is a bloging site for rowmari..you can blog here..!!</p>
    <p> 
        <a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> 
        <a class="btn btn-info btn-lg" role="button" href="/register">Register</a>
    </p>

</div>
    
@endsection

