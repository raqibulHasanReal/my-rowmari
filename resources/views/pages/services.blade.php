@extends('layouts.app')

@section('content')

<div id="introblocks">
    <ul class="nospace group">
        <li class="">
            <article>
                <div>
                    <h6 class="heading">Lacinia vivamus lorem</h6>
                    <p>Ex mauris faucibus libero sed maximus lobortis nunc luctus nisi luctus varius convallis [&hellip;]</p>
                </div>
                <img src="images/demo/320x180.png" alt="">
                <footer><a href="#">More Details</a></footer>
            </article>
        </li>
    </ul>
</div>

@endsection