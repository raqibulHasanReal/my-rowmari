@extends('layouts.app')

@section('content')

<div class="animatebox animate fadeIn">animate fadeIn</div>
<div class="animatebox animate fadeInUp">animate fadeInUp</div>
<div class="animatebox animate fadeInUpBig">animate fadeInUpBig</div>
<div class="animatebox animate fadeInDown">animate fadeInDown</div>
<div class="animatebox animate fadeInDownBig">animate fadeInDownBig</div>
<div class="animatebox animate fadeInLeft">animate fadeInLeft</div>
<div class="animatebox animate fadeInLeftBig">animate fadeInLeftBig</div>
<div class="animatebox animate fadeInRight">animate fadeInRight</div>
<div class="animatebox animate fadeInRightBig">animate fadeInRightBig</div>
<div class="animatebox animate flip">animate flip</div>
<div class="animatebox animate flipInX">animate flipInX</div>
<div class="animatebox animate flipInY">animate flipInY</div>
<div class="animatebox animate lightSpeedIn">animate lightSpeedIn</div>
<div class="animatebox animate rotateIn">animate rotateIn</div>
<div class="animatebox animate rotateInDownLeft">animate rotateInDownLeft</div>
<div class="animatebox animate rotateInDownRight">animate rotateInDownRight</div>
<div class="animatebox animate rotateInUpLeft">animate rotateInUpLeft</div>
<div class="animatebox animate rotateInUpRight">animate rotateInUpRight</div>
<div class="animatebox animate slideInDown">animate slideInDown</div>
<div class="animatebox animate slideInRight">animate slideInRight</div>
<div class="animatebox animate slideInLeft">animate slideInLeft</div>
<div class="animatebox animate rollIn">animate rollIn</div>
<div class="animatebox animate bounce">animate bounce</div>
<div class="animatebox animate flash">animate flash</div>
<div class="animatebox animate shake">animate shake</div>
<div class="animatebox animate swing">
    <img src="https://www.wikivillage.in/img/slide3.jpg" alt="">
</div>
<div class="animatebox animate wobble">animate wobble</div>
    
@endsection

    