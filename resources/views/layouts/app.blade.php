<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     {{-- Meta --}}
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     
    {{-- Bootstrap Core CSS --}}
	{{--  <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" rel="stylesheet">  --}}

    {{-- Template CSS  --}}
    <link rel="stylesheet" href="{{asset('css/animate.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/nexus.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}" rel="stylesheet">
    
     {{-- Google Fonts --}}
	<link href="http://fonts.googleapis.com/css?family=Lato:400,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" rel="stylesheet" type="text/css">
    
     {{-- Favicon --}}
	{{--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">  --}}
</head>
<body>
        @include('inc.navbar')
        <div class="container margin_top_content">
            @include('inc.massages')
            @yield('content')
        </div>   
        
        @stack('js')
        <script src="{!! asset('js/jquery.min.js') !!}"></script>
        <script src="{!! asset('js/jquery.visible.js') !!}"></script>

</body>
</html>
