@extends('layouts.app')

@section('content')
@if(count($posts) > 0)
@foreach($posts as $post)
<div id="content" class="container">
    <div class="row margin-vert-30">
        <!-- Main Column -->
        <div class="col-md-9">
            <!-- Blog Post -->
            
            <div class="blog-post padding-bottom-20">
                <!-- Blog Item Header -->
                
                <div class="blog-item-header">
                    <!-- Date -->
                    <div class="blog-post-date pull-left">
                        <span class="day">{{ $post->created_at->format('d') }}</span>
                        <span class="month">{{ $post->created_at->format('M') }}</span>
                    </div>
                    <!-- End Date -->
                    <!-- Title -->
                    <h2>
                        <a href="/posts/{{$post->id}}"> {{$post->title}} </a>
                    </h2>
                    <div class="clearfix"></div>
                    <!-- End Title -->
                </div>
                
                <!-- End Blog Item Header -->
                <!-- Blog Item Details -->
                
                <div class="blog-post-details">
                    <!-- Author Name -->
                    
                    <div class="blog-post-details-item blog-post-details-item-left user-icon">
                        <i class="fa fa-user"></i>
                        <a href="#">{{ $post->user->name }}</a>
                    </div>
                    
                    <!-- End Author Name -->
                    <!-- Tags -->
                    
                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-tags tags-icon">
                        <i class="fa fa-tag"></i>
                        <a href="#">jQuery</a> ,
                        <a href="#">CSS</a> ,
                        <a href="#">Grunt</a>
                    </div>
                    
                    <!-- End Tags -->
                    <!-- # of Comments -->
                    
                    <div
                    class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last comments-icon">
                    <a href="/posts/{{$post->id}}">
                        <i class="fa fa-comments"></i>
                        {{count($post->comments)}} comments
                    </a>
                </div>
                
                <!-- End # of Comments -->
            </div>
            
            <!-- End Blog Item Details -->
            <!-- Blog Item Body -->
            
            <div class="blog">
                <div class="clearfix"></div>
                <div class="blog-post-body row margin-top-15">
                    <div class="col-md-5">
                    <img class="pull-left" src="/storage/cover_images/{{$post->cover_image}}" alt="thumb1">
                    </div>
                    <div class="col-md-7">
                        <div class="post_body_less">
                            <p>{!! $post->body !!}</p>
                        </div>
                        <!-- Read More -->
                        <a href="/posts/{{$post->id}}" class="btn btn-primary">
                            Read More <i class="icon-chevron-right readmore-icon"></i>
                        </a>
                        <!-- End Read More -->
                    </div>
                </div>
            </div>
            
            <!-- End Blog Item Body -->
        </div>
        
        <!-- End Blog Item -->                    
    </div>
    <!-- End Main Column -->
    </div>
</div>
@endforeach
<div class="container">
    <div class="row margin-vert-30">
        <div class="col-md-9">
            <div class="pull-right">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</div>
@else
<p>No Posts found</p>
@endif
<!-- Side Column -->

<div class="col-md-3">
    
    
</div>

<!-- === END CONTENT === -->
    
    @endsection