@extends('layouts.app')

@section('content')

<div id="content" class="container">
    <div class="row margin-vert-30">
        <!-- Main Column -->
        <div class="col-md-9">
            <div class="blog-post">
                <div class="blog-item-header">
                    <div class="blog-post-date pull-left">
                        <span class="day">{{ $post->created_at->format('d') }}</span>
                        <span class="month">{{ $post->created_at->format('M') }}</span>
                    </div>
                    <h2>
                        <a>
                            {{$post->title}}
                        </a>
                    </h2>
                </div>
                
                <div class="blog-post-details">
                    <!-- Author Name -->
                    <div class="blog-post-details-item blog-post-details-item-left user-icon">
                        <i class="fa fa-user"></i>
                        <a href="#">{{ $post->user->name }}</a>
                    </div>
                    <!-- End Author Name -->
                    <!-- # of Comments -->
                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last comments-icon">
                        <a href="#comment">
                            <i class="fa fa-comments"></i>
                            {{count($post->comments)}} comments
                        </a>
                    </div>
                    @if($post->user->id === auth()->user()->id)                                                       
                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last comments-icon">
                        <a href="/posts/{{$post->id}}/edit">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                    </div>
                    <div class="blog-post-details-item blog-post-details-item-left blog-post-details-item-last comments-icon">
                        <a href="" data-toggle="modal" data-target="#modalForDelete">
                            <i class="fa fa-trash-o"></i>
                            Delete
                        </a>
                    </div>
                    @endif
                    <!-- End # of Comments -->
                </div>
                
                <!-- Modal -->
                <div class="modal fade margin_top_content" id="modalForDelete" tabindex="-1" role="dialog" aria-labelledby="modalForDeleteLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="modalForDeleteLabel">Delete panel</h4>
                            </div>
                            <div class="modal-body text-center">
                                <h4>Do you want delete this post ?</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                {{--  <button type="button" class="btn btn-danger">Yes</button>  --}}
                                {!!Form::open(['action' => ['PostsController@destroy',$post->id],'method'=>'POST'])!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Yes',['class' => 'btn btn-danger'])}}
                                {!!Form::close()!!}
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="blog-item">
                    <div class="clearfix"></div>
                    
                    <div class="blog-post-body row margin-top-15">
                        <div class="col-md-12">
                            <img class="pull-left margin-bottom-30" src="/storage/cover_images/{{$post->cover_image}}" alt="image1">     
                        </div>
                        <div class="col-md-12">
                            {!! $post->body !!}
                        </div>
                    </div>
                    
                    <div class="blog-item-footer">
                        <!-- About the Author -->
                        <div class="blog-author card card-default margin-bottom-30 animate shake">
                            <div class="card-header">
                                <h3>About the Author</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <img class="pull-left" src="https://www.wikivillage.in/img/slide3.jpg" alt="image1">
                                    </div>
                                    <div class="col-md-10">
                                        <label><b> {{ $post->user->name }}</b></label>
                                        <p>Lorem ipsum dolor sit amet, in pri offendit ocurreret. Vix sumo ferri an. pfs adodio fugit delenit ut qui. Omittam suscipiantur ex vel,ex audiam intellegat gfIn labitur discere eos, nam an feugiat voluptua.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- End About the Author -->
                        
                        <!-- Comments -->
                        <div id="comment" class="blog-recent-comments card card-default margin-bottom-30">
                            <div class="card-header">
                                <h3>Comments</h3>
                            </div>
                            <ul class="list-group">
                                @foreach($post->comments as $comment)
                                <li class="list-group-item">
                                    <div class="row animate swing">
                                        <div class="col-md-2 profile-thumb">
                                            <a  href="#">
                                                <img class="media-object" src="https://www.wikivillage.in/img/slide3.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="col-md-10">
                                            <h4>{{ $comment->user->name }}</h4>
                                            <p>{{ $comment->body }}</p>
                                            <span class="date"><i class="fa fa-clock-o"></i> {{ $comment->created_at->diffForHumans() }}</span>
                                            @if($comment->user->id === auth()->user()->id)         
                                            <div class="">
                                                <button class="btn btn-sm btn-info">edit</button>
                                                <button data-toggle="modal" data-target="#deleteComment" class="btn btn-sm btn-danger">delete</button>
                                            </div>      
                                            @endif
                                        </div>
                                    </div>
                                </li>
                                
                                <!-- Modal for comment delete-->
                                <div class="modal fade margin_top_content" id="deleteComment" tabindex="-1" role="dialog" aria-labelledby="modalForDeleteLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="modalForDeleteLabel">Delete panel</h4>
                                            </div>
                                            <div class="modal-body text-center">
                                                <h4>Do you want delete this comment ?</h4>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                                {{--  <button type="button" class="btn btn-danger">Yes</button>  --}}
                                                {!!Form::open(['action' => ['CommentsController@destroy',$comment->id],'method'=>'POST'])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                                {{Form::submit('Yes',['class' => 'btn btn-danger'])}}
                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                                
                                
                                <!-- Comment Form -->
                                <li class="list-group-item">
                                    <div class="blog-comment-form">
                                        <div class="row margin-top-20">
                                            <div class="col-md-12">
                                                <div class="pull-left">
                                                    <h3>Leave a Comment</h3>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row margin-top-20">
                                            <div class="col-md-12">
                                                {!! Form::open(['action'=>'CommentsController@store', 'method' => 'POST']) !!}
                                                <input type="hidden" name="post_id" value={{ $post->id }}>
                                                
                                                
                                                {{--  <label>Message</label>  --}}
                                                <div class="row margin-bottom-20">
                                                    <div class="col-md-11 col-md-offset-0">
                                                        <textarea name="comment" class="form-control" rows="8" required></textarea>
                                                    </div>
                                                </div>
                                                
                                                <p><button class="btn btn-primary" type="submit">Send Comment</button></p>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <!-- End Comment Form -->
                            </ul>  
                        </div>
                        <!-- End Comments -->
                    </div> 
                </div>
            </div>
        </div>   
        <!-- End Blog Post -->		
        <!-- End Main Column -->
        <!-- Side Column -->  
        <div class="col-md-3">
            <!-- Blog Tags -->
            <div class="blog-tags">
                <h3>Tags</h3>
                <ul class="blog-tags">
                    <li><a href="#" class="blog-tag">HTML</a></li>
                    <li><a href="#" class="blog-tag">CSS</a></li>
                    <li><a href="#" class="blog-tag">JavaScript</a></li>
                    <li><a href="#" class="blog-tag">jQuery</a></li>
                    <li><a href="#" class="blog-tag">PHP</a></li>
                    <li><a href="#" class="blog-tag">Ruby</a></li>
                    <li><a href="#" class="blog-tag">CoffeeScript</a></li>
                    <li><a href="#" class="blog-tag">Grunt</a></li>
                    <li><a href="#" class="blog-tag">Bootstrap</a></li>
                    <li><a href="#" class="blog-tag">HTML5</a></li>
                </ul>			</div>
                <!-- End Blog Tags -->
                <!-- Recent Posts -->
                <div class="recent-posts">
                    <h3>Recent Posts</h3>
                    <ul class="posts-list margin-top-10">
                        <li>
                            <div class="recent-post">
                                @foreach ($posts as $post)
                                <div class="row">
                                    <div class="col-md-5">
                                        <a href="/posts/{{$post->id}}">
                                            <img class="pull-left media-object" src="/storage/cover_images/{{$post->cover_image}}" alt="thumb1">
                                        </a>
                                        
                                    </div>
                                    <div class="col-md-7">
                                        <div class="description_area_less">
                                            
                                            <a href="/posts/{{$post->id}}" class="posts-list-title">{{$post->title}}</a>
                                        </div>
                                        <br>
                                        <span class="recent-post-date">
                                            {{$post->created_at->diffForHumans()}}
                                        </span>
                                    </div>
                                    
                                </div>
                                
                                @endforeach
                            </div>
                            <div class="clearfix"></div>
                        </li>                             
                    </ul>			
                </div>
                <!-- End Recent Posts -->
                <!-- End Side Column -->
            </div>
        </div>
    </div>
    
    @endsection
    
    