@extends('layouts.app')

@section('content')
<div class="col-md-8 offset-md-2">
    <div class="card">
        <div class="card-body">
            <div class="text-center">
                <h1>Edit Your Post</h1>
            </div>
            {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-group">
                {{Form::label('title', 'Title')}}
                {{Form::text('title',$post->title,['class' => 'form-control', 'placeholder' => 'Title', 'required'])}}
            </div>
            <div class="form-group">
                {{Form::label('image', 'Image')}}
                <input type="file" value={{$post->cover_image}} accept="image/*" name="cover_image">
            </div>
            <div class="form-group">
                {{Form::label('body', 'Body')}}
                {{Form::textarea('body',$post->body,['id' => 'foo','class' => 'form-control','required', 'placeholder' => 'pls write your blog'])}}
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    {{Form::hidden('_method','PUT')}}
                    {{Form::submit('Update this blog', ['class' => 'btn btn-primary'])}}
                </div>
                <div class="col-md-6">
                    <a href="/posts/{{$post->id}}">
                    <button type="button" class="btn btn-info"> Close</button></a>
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@endsection

@push('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'foo' );
</script>
@endpush