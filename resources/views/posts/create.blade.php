@extends('layouts.app')

@section('content')

    <div class="text-center">
        <h1>Write Your Post</h1>
    </div>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title','',['class' => 'form-control', 'placeholder' => 'Title', 'required'])}}
        </div>
        <div class="form-group">
            <label for="" class="form-label">Image</label>
            {{-- <button class="btn btn-default" onclick="document.getElementById('img-up').click()"><i class="fa fa-picture-o"></i></button>
            <input type="file" name="cover_image" id="img-up" class="d-none"> --}}
            <input type="file" accept="image/*" name="cover_image">
        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body','',['id' => 'foo','class' => 'form-control','required', 'placeholder' => 'pls write your blog'])}}
        </div>
        <div class="margin-bottom-30">
            {{Form::submit('Submit this blog', ['class' => 'btn btn-primary'])}}
        </div>
    {!! Form::close() !!}
    
@endsection
    
@push('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'foo' );
</script>
@endpush